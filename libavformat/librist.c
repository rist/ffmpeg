/*
 * This file is part of FFmpeg.
 *
 * FFmpeg is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * FFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FFmpeg; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/**
 * @file
 * Reliable Internet Streaming Transport protocol
 */


#include "libavutil/avassert.h"
#include "libavutil/opt.h"
#include "libavutil/parseutils.h"
#include "libavutil/time.h"

#include "avformat.h"
#include "internal.h"
#include "network.h"
#include "os_support.h"
#include "url.h"

#include <librist/librist.h>

typedef struct RISTContext {
    const AVClass *class;

} RISTContext;

#define D AV_OPT_FLAG_DECODING_PARAM
#define E AV_OPT_FLAG_ENCODING_PARAM
#define OFFSET(x) offsetof(RISTContext, x)
static const AVOption librist_options[] = {
    { NULL }
};


static int librist_open(URLContext *h, const char *uri, int flags)
{
    RISTContext *s = h->priv_data;

    return 0;
}

static int librist_read(URLContext *h, uint8_t *buf, int size)
{
    RISTContext *s = h->priv_data;

    return 0;
}

static int librist_write(URLContext *h, const uint8_t *buf, int size)
{
    RISTContext *s = h->priv_data;
    int ret;

    return 0;
}

static int librist_close(URLContext *h)
{
    RISTContext *s = h->priv_data;

    return 0;
}

static const AVClass librist_class = {
    .class_name = "librist",
    .item_name  = av_default_item_name,
    .option     = librist_options,
    .version    = LIBAVUTIL_VERSION_INT,
};

const URLProtocol ff_librist_protocol = {
    .name                = "rist",
    .url_open            = librist_open,
    .url_read            = librist_read,
    .url_write           = librist_write,
    .url_close           = librist_close,
    .priv_data_size      = sizeof(RISTContext),
    .flags               = URL_PROTOCOL_FLAG_NETWORK,
    .priv_data_class     = &librist_class,
};
